import os
from flask_unchained import BundleConfig


class Config(BundleConfig):
    CRYPTOCOINDATA_HOST = os.getenv(
        'CRYPTOCOINDATA_HOST', 'https://cryptocoindata.chriamue.de')
