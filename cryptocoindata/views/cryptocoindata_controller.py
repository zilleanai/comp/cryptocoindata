import os
import pandas as pd
import requests
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import Controller, route, injectable
from backend.config import Config as AppConfig
from ..config import Config


class CryptoCoinDataController(Controller):

    @route('/platforms', methods=['GET'])
    def platforms(self):
        print('platforms')
        host = Config.CRYPTOCOINDATA_HOST + '/api/v1/platforms'
        r = requests.get(host).json()
        resp = jsonify(r)
        return resp

    @route('/features/<string:platform>', methods=['GET'])
    def features(self, platform):
        host = Config.CRYPTOCOINDATA_HOST + '/api/v1/features/' + platform
        r = requests.get(host).json()
        r['platform'] = platform
        resp = jsonify(r)
        return resp

    @route('/records/years/<string:platform>', methods=['GET'])
    def records_years(self, platform):
        host = Config.CRYPTOCOINDATA_HOST + '/api/v1/records/years/' + platform
        r = requests.get(host).json()
        r['platform'] = platform
        resp = jsonify(r)
        return resp

    @route('/records/<int:year>/months/<string:platform>', methods=['GET'])
    def records_months(self, year, platform):
        host = Config.CRYPTOCOINDATA_HOST + '/api/v1/records/' + \
            str(year) + '/months/' + platform
        r = requests.get(host).json()
        r['year'] = year
        r['platform'] = platform
        resp = jsonify(r)
        return resp

    @route('/records/<int:year>/<int:month>/<string:platform>', methods=['GET'])
    def records_year_month(self, year, month, platform):
        host = Config.CRYPTOCOINDATA_HOST + '/api/v1/records/' + \
            str(year) + '/' + '{:02d}'.format(month) + '/' + platform
        r = requests.get(host).json()
        r['year'] = year
        r['month'] = month
        r['platform'] = platform
        resp = jsonify(r)
        return resp

    @route('/start-download', methods=['POST'])
    def start_download(self):
        project = request.json['project']
        platform = request.json['platform']
        downloads = request.json['downloads'].split(',')
        for download in downloads:
            url = Config.CRYPTOCOINDATA_HOST + '/' + download
            filename = os.path.join(
                AppConfig.DATA_FOLDER, project, os.path.basename(download))
            print(filename)
            if not os.path.exists(filename):
                r = requests.get(url, stream=True)
                with open(filename, "wb") as handle:
                    for data in r.iter_content():
                        handle.write(data)
        resp = jsonify(success=True)
        return resp
