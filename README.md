# cryptocoindata

Component for fetching data for cryptocoins.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/cryptocoindata
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.cryptocoindata',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.cryptocoindata.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  DownloadRecord
} from 'comps/cryptocoindata/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  DownloadRecord: 'DownloadRecord',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.DownloadRecord,
    path: '/downloadrecord',
    component: DownloadRecord,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.DownloadRecord} />
    ...
</div>
```
