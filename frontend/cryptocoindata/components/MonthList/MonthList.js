import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listMonths } from 'comps/cryptocoindata/actions'
import { selectMonthsList } from 'comps/cryptocoindata/reducers/months'
import Checkbox from 'components/Checkbox';
import { RecordList } from 'comps/cryptocoindata/components';

import './month-list.scss'

class MonthList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      records: new Map(),
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const checkedItems = this.state.checkedItems.set(item, isChecked)
    this.setState({ checkedItems });
    this.props.onChange({ value: this.selectedToIndices(checkedItems), records: this.state.records });
  }

  handleRecords = ({ value: value, records: records }) => {
    const s_records = this.state.records
    let r = new Map(function* () { yield* s_records; yield* records; }());
    this.props.onChange({ value: this.selectedToIndices(this.state.checkedItems), records: r });
    this.setState({ records: r });
  }

  componentWillMount() {
    const { listMonths, platform, year } = this.props
    listMonths.maybeTrigger({ platform, year })
  }

  selectedToIndices(checkedItems) {
    var ids = []
    checkedItems.forEach(function (value, key) {
      if (value == true) {
        ids.push(key)
      }
    }, checkedItems)
    return ids
  }

  render() {
    const { isLoaded, error, year, months, platform } = this.props
    if (!isLoaded) {
      return (<div>{platform + year}</div>)
    }
    return (
      <div>
        <ul className="months" >
          <div>
            {months.map((month, i) => {
              const active = this.state.checkedItems.get(month.toString())
              return (
                <li key={'month' + i} className={classnames('months-link', { active })} onClick={this.handleChange}>
                  <Checkbox name={month.toString()} checked={this.state.checkedItems.get(month.toString())} onChange={this.handleChange} />
                  {month + " "}
                  {active ?
                    <RecordList platform={platform} year={year} month={month.toString()} onChange={this.handleRecords} /> : null}
                </li>
              )
            }
            )}
          </div>
        </ul>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/cryptocoindata/reducers/months'))

const withSaga = injectSagas(require('comps/cryptocoindata/sagas/months'))

const withConnect = connect(
  (state, props) => {
    const year = props.year
    const months = selectMonthsList(state, year)
    const isLoaded = !!months
    return {
      months,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listMonths }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(MonthList)
