import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listRecords } from 'comps/cryptocoindata/actions'
import { selectRecordsList } from 'comps/cryptocoindata/reducers/records'
import Checkbox from 'components/Checkbox';


import './record-list.scss'

class RecordList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      platform: props.platform,
      year: props.year,
      month: props.month,
      downloads: []
    }
  }

  handleChange = (e) => {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const checkedItems = this.state.checkedItems.set(item, isChecked)
    this.setState({ checkedItems });
    const value = this.selectedToIndices(checkedItems)
    this.props.onChange({ value: value, records: checkedItems });
  }

  componentWillMount() {
    const { listRecords, platform, year, month } = this.props
    listRecords.maybeTrigger({ platform, year, month })
  }

  selectedToIndices(checkedItems) {
    var ids = []
    checkedItems.forEach(function (value, key) {
      if (value == true) {
        ids.push(key)
      }
    }, checkedItems)
    return ids
  }

  render() {
    const { isLoaded, error, year, month, records, platform } = this.props
    if(error){
      return (error)
    }
    if (!isLoaded) {
      return (<div>{platform + year + month}</div>)
    }
    return (
      <div>
        <ul className="records" >
          <div>
            {Object.keys(records).map((key, i) => {
              const record = records[key]
              const active = this.state.checkedItems.get(year.toString())
              return (
                <li key={'record'+i} className={classnames('record-link', { active })} onClick={this.handleChange}>
                  <Checkbox name={record.toString()} checked={this.state.checkedItems.get(record.toString())} onChange={this.handleChange} />
                  {key + " "}
                </li>
              )
            }
            )}
          </div>
        </ul>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/cryptocoindata/reducers/records'))

const withSaga = injectSagas(require('comps/cryptocoindata/sagas/records'))

const withConnect = connect(
  (state, props) => {
    const platform = props.platform
    const year = props.year
    const month = parseInt(props.month)
    const records = selectRecordsList(state, platform, year, month)

    const isLoaded = !!records

    return {
      platform,
      year,
      month,
      records,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listRecords }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(RecordList)
