import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listYears } from 'comps/cryptocoindata/actions'
import { selectYearsList } from 'comps/cryptocoindata/reducers/years'
import Checkbox from 'components/Checkbox';
import { MonthList } from 'comps/cryptocoindata/components';

import './year-list.scss'

class YearList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      platform: props.platform,
      months: [],
      records: new Map(),
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const checkedItems = this.state.checkedItems.set(item, isChecked)
    this.setState({ checkedItems });
    this.props.onChange({ value: this.selectedToIndices(checkedItems), records: this.state.records });
  }

  handleMonths = ({ value: value, records: records }) => {
    const s_records = this.state.records
    let r = new Map(function* () { yield* s_records; yield* records; }());
    this.props.onChange({ value: this.selectedToIndices(this.state.checkedItems), records: r });
    this.setState({ months: value, records: r });
  }

  componentDidMount() {
    const { platform } = this.props
    listYears.maybeTrigger({ platform })
  }

  componentWillReceiveProps(nextProps) {
    const { listYears, platform, years } = nextProps
    if (platform != this.props.platform) {
      listYears.maybeTrigger({ platform })
    }
    if (nextProps.isLoaded) {
      this.setState({
        platform,
        years,
      });
    }
  }

  selectedToIndices(checkedItems) {
    var ids = []
    checkedItems.forEach(function (value, key) {
      if (value == true) {
        ids.push(key)
      }
    }, checkedItems)
    return ids
  }

  render() {
    const { isLoaded, error, years, platform } = this.props
    if (!isLoaded) {
      return (<div>{platform}</div>)
    }
    return (
      <div>
        <ul className="years" >
          <div>
            {years.years.map((year, i) => {
              const active = this.state.checkedItems.get(year.toString())
              return (
                <li key={i} className={classnames('year-link', { active })} onClick={this.handleMonths}>
                  <Checkbox name={year.toString()} checked={this.state.checkedItems.get(year.toString())} onChange={this.handleChange} />
                  {year + " "}
                  {this.state.checkedItems.get(year.toString()) && <MonthList platform={platform} year={year.toString()} onChange={this.handleMonths} />}
                </li>
              )
            }
            )}
          </div>
        </ul>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/cryptocoindata/reducers/years'))

const withSaga = injectSagas(require('comps/cryptocoindata/sagas/years'))

const withConnect = connect(
  (state, props) => {
    const platform = props.platform
    const years = selectYearsList(state, platform)
    const isLoaded = !!years
    return {
      years,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listYears }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(YearList)
