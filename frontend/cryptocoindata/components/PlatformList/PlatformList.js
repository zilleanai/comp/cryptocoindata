import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'

import { listPlatforms } from 'comps/cryptocoindata/actions'
import { selectPlatformsList } from 'comps/cryptocoindata/reducers/platforms'
import Select from 'react-select';


import './platform-list.scss'

class PlatformList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    this.props.onChange({value:selectedOption.value})
  }

  componentWillMount() {
    this.props.listPlatforms.maybeTrigger()
  }

  platformsToOptions(platforms) {
    var options = []
    platforms.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, platforms)
    return options
  }

  render() {
    const { isLoaded, error, platforms } = this.props
    if (!isLoaded) {
      return null
    }
    const options = this.platformsToOptions(platforms.platforms)
    return (
      <Select
        value={this.state.selectedOption}
        onChange={this.handleChange}
        defaultValue={options[0]}
        name="platforms"
        options={options}
        className="basic-multi-select"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/cryptocoindata/reducers/platforms'))

const withSaga = injectSagas(require('comps/cryptocoindata/sagas/platforms'))

const withConnect = connect(
  (state) => {
    const platforms = selectPlatformsList(state)
    const isLoaded = !!platforms
    return {
      platforms,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listPlatforms }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PlatformList)
