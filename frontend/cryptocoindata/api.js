import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function cryptocoindata(uri) {
  return v1(`/cryptocoindata${uri}`)
}

export default class Cryptocoindata {

  static listPlatforms() {
    return get(cryptocoindata('/platforms'))
  }

  static listYears(platform) {
    return get(cryptocoindata(`/records/years/${platform}`))
  }

  static listMonths(payload) {
    return get(cryptocoindata(`/records/${payload.year}/months/${payload.platform}`))
  }

  static listRecords({platform, year, month }) {
    return get(cryptocoindata(`/records/${year}/${month}/${platform}`))
  }

  static startDownload({ platform, downloads }) {
    return post(cryptocoindata(`/start-download?project`), {
      project: storage.getProject(), platform, downloads: downloads.join()
    })
  }

}
