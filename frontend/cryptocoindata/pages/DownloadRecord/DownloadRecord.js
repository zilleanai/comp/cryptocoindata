import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Moment from 'moment'
import momentLocalizer from 'react-widgets-moment';
import { DateTimePicker } from 'react-widgets'
import { Field, reduxForm, formValues, change } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { startDownload } from 'comps/cryptocoindata/actions'
import { InfoBox, DangerAlert, PageContent, CheckboxList } from 'components'
import { PlatformList, YearList } from 'comps/cryptocoindata/components'
import { TextField } from 'components/Form'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import classnames from 'classnames'
import './download-record.scss'
const FORM_NAME = 'startDownload'

class DownloadRecord extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      years: [],
      platform: '',
      records: new Map(),
      downloads: [],
    }
  }

  renderPlatforms = ({ input: { onChange } }) => {
    return (<PlatformList
      onChange={onChange} />)
  }

  renderYears = ({ input: { onChange }, platform }) => {
    return (<YearList platform={platform} onChange={onChange} />)
  }

  handlePlatforms = ({ value: platform }) => {
    this.setState({ platform: platform });
  }

  handleYears = ({ value: years, records: records }) => {
    const s_records = this.state.records
    let r = new Map(function* () { yield* s_records; yield* records; }());
    let downloads = this.selectedToIndices(r)
    this.setState({ years, records: r, downloads: downloads });
  }

  selectedToIndices(checkedItems) {
    var ids = []
    checkedItems.forEach(function (value, key) {
      if (value == true) {
        ids.push(key)
      }
    }, checkedItems)
    return ids
  }


  render() {
    const { isLoaded, error, handleSubmit, pristine, submitting } = this.props
    if (!isLoaded) {
      return null
    }
    this.props.change('downloads', this.state.downloads) // hidden field: downloads

    return (
      <PageContent>
        <Helmet>
          <title>Download Record</title>
        </Helmet>
        <h1>Download Record!</h1>
        {error && <DangerAlert>{error}</DangerAlert>}
        <form onSubmit={handleSubmit(startDownload)}>
          <h5>Platforms</h5>
          <Field name='platform' component={this.renderPlatforms} onChange={this.handlePlatforms} />
          <h5>Years</h5>
          <Field
            name="years"
            component={this.renderYears}
            onChange={this.handleYears}
            items={this.state.years}
            platform={this.state.platform}
            value={this.state.years}
          />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Downloading...' : 'Download'}
            </button>
          </div>
        </form>
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state) => {
    const isLoaded = true
    if (isLoaded) {
      return {
        isLoaded
      }
    } else {
      return {
        isLoaded: false
      }
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch) => {
    dispatch(reset(FORM_NAME))
  }
})

const withSaga = injectSagas(require('comps/cryptocoindata/sagas/startDownload'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(DownloadRecord)
