import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listYears } from 'comps/cryptocoindata/actions'
import CryptocoindataApi from 'comps/cryptocoindata/api'
import { selectYears } from 'comps/cryptocoindata/reducers/years'

export const KEY = 'years'


export const maybeListYearsSaga = function *({payload}) {
  const { byPlatform, isLoading } = yield select(selectYears)
  const isLoaded = !!byPlatform[payload.platform]
  if (!(isLoaded || isLoading)) {
    yield put(listYears.trigger(payload))
  }
}

export const listYearsSaga = createRoutineSaga(
  listYears,
  function *successGenerator(payload) {
    const years = yield call(CryptocoindataApi.listYears, payload.platform)
    yield put(listYears.success({
      years,
    }))
  },
)

export default () => [
  takeEvery(listYears.MAYBE_TRIGGER, maybeListYearsSaga),
  takeLatest(listYears.TRIGGER, listYearsSaga),
]
