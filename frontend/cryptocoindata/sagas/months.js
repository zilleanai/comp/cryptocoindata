import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listMonths } from 'comps/cryptocoindata/actions'
import CryptocoindataApi from 'comps/cryptocoindata/api'
import { selectMonths } from 'comps/cryptocoindata/reducers/months'

export const KEY = 'months'


export const maybeListMonthsSaga = function* ({payload}) {
  const { byYear, isLoading } = yield select(selectMonths)
  const isLoaded = !!byYear[payload.year]
  if (!(isLoaded || isLoading)) {
    yield put(listMonths.trigger(payload))
  }
}

export const listMonthsSaga = createRoutineSaga(
  listMonths,
  function* successGenerator(payload) {
    const months = yield call(CryptocoindataApi.listMonths, payload)
    yield put(listMonths.success({
      months,
    }))
  },
)

export default () => [
  takeEvery(listMonths.MAYBE_TRIGGER, maybeListMonthsSaga),
  takeLatest(listMonths.TRIGGER, listMonthsSaga),
]
