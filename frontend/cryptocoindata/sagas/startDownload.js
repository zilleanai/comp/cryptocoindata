import { call, put, takeLatest } from 'redux-saga/effects'

import { startDownload } from 'comps/cryptocoindata/actions'
import { createRoutineFormSaga } from 'sagas'
import CryptocoindataApi from 'comps/cryptocoindata/api'

export const KEY = 'start_download'

export const startDownloadSaga = createRoutineFormSaga(
  startDownload,
  function* successGenerator(payload) {
    const response = yield call(CryptocoindataApi.startDownload, payload)
    yield put(startDownload.success(response))
  }
)

export default () => [
  takeLatest(startDownload.TRIGGER, startDownloadSaga)
]
