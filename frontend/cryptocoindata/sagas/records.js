import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listRecords } from 'comps/cryptocoindata/actions'
import CryptocoindataApi from 'comps/cryptocoindata/api'
import { selectRecords } from 'comps/cryptocoindata/reducers/records'

export const KEY = 'records'


export const maybeListRecordsSaga = function* ( {payload} ) {
  const { byRecordId, isLoading } = yield select(selectRecords)
  const { platform, year, month } = payload
  const recordId = '' + platform + year + month
  const isLoaded = !!byRecordId[recordId]
  if (!(isLoaded || isLoading)) {
    yield put(listRecords.trigger(payload))
  }
}

export const listRecordsSaga = createRoutineSaga(
  listRecords,
  function* successGenerator(payload) {
    const { platform, year, month } = payload
    const records = yield call(CryptocoindataApi.listRecords, { platform, year, month })
    yield put(listRecords.success({
      records,
    }))
  },
)

export default () => [
  takeEvery(listRecords.MAYBE_TRIGGER, maybeListRecordsSaga),
  takeLatest(listRecords.TRIGGER, listRecordsSaga),
]
