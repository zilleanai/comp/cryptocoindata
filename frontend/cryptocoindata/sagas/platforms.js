import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listPlatforms } from 'comps/cryptocoindata/actions'
import CryptocoindataApi from 'comps/cryptocoindata/api'
import { selectPlatforms } from 'comps/cryptocoindata/reducers/platforms'

export const KEY = 'platforms'


export const maybeListPlatformsSaga = function *() {
  const { isLoading, isLoaded } = yield select(selectPlatforms)
  if (!(isLoaded || isLoading)) {
    yield put(listPlatforms.trigger())
  }
}

export const listPlatformsSaga = createRoutineSaga(
  listPlatforms,
  function *successGenerator() {
    const platforms = yield call(CryptocoindataApi.listPlatforms)
    yield put(listPlatforms.success({
      platforms,
    }))
  },
)

export default () => [
  takeEvery(listPlatforms.MAYBE_TRIGGER, maybeListPlatformsSaga),
  takeLatest(listPlatforms.TRIGGER, listPlatformsSaga),
]
