import { listYears } from 'comps/cryptocoindata/actions'


export const KEY = 'years'

const initialState = {
  isLoading: false,
  isLoaded: false,
  platforms: [],
  byPlatform: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { platforms, byPlatform } = state
  switch (type) {
    case listYears.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listYears.SUCCESS:
      if (!platforms.includes(payload.years.platform)) {

        platforms.push(payload.years.platform)
      }
      byPlatform[payload.years.platform] = payload.years
      return {
        ...state,
        years: payload.years,
        isLoaded: true,
      }

    case listYears.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listYears.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectYears = (state) => state[KEY]
export const selectYearsList = (state, platform) => selectYears(state).byPlatform[platform]
