import { listPlatforms } from 'comps/cryptocoindata/actions'


export const KEY = 'platforms'

const initialState = {
  isLoading: false,
  isLoaded: false,
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { platforms } = payload || {}
  switch (type) {
    case listPlatforms.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listPlatforms.SUCCESS:
      return {
        ...state,
        platforms,
        isLoaded: true,
      }

    case listPlatforms.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listPlatforms.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPlatforms = (state) => state[KEY]
export const selectPlatformsList = (state) => {
  const platforms = selectPlatforms(state)
  return platforms.platforms
}
