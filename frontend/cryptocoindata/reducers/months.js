import { listMonths } from 'comps/cryptocoindata/actions'


export const KEY = 'months'

const initialState = {
  isLoading: false,
  isLoaded: false,
  years: [],
  byYear: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const {months} = payload || {}
  const { years, byYear } = state
  switch (type) {
    case listMonths.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listMonths.SUCCESS:
      if (!years.includes(months.year)) {

        years.push(months.year)
      }
      byYear[months.year] = months.months
      return {
        ...state,
        months: months.months,
        isLoaded: true,
      }

    case listMonths.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listMonths.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectMonths = (state) => state[KEY]
export const selectMonthsList = (state, year) => selectMonths(state).byYear[year]
