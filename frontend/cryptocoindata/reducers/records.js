import { listRecords } from 'comps/cryptocoindata/actions'


export const KEY = 'records'

const initialState = {
  isLoading: false,
  isLoaded: false,
  recordIds: [],
  byRecordId: {},
  error: null,
}

const recordId = (platform, year, month) => {
  return ""+platform + year + month
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { records } = payload || {}
  const { recordIds, byRecordId } = state
  switch (type) {
    case listRecords.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listRecords.SUCCESS:
      const recordID = recordId(records.platform,records.year,records.month)
      if (!recordIds.includes(recordID)) {

        recordIds.push(recordID)
      }
      byRecordId[recordID] = records.records
      return {
        ...state,
        records: records.records,
        isLoaded: true,
      }

    case listRecords.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listRecords.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectRecords = (state) => state[KEY]
export const selectRecordsList = (state, platform, year, month) => selectRecords(state).byRecordId[recordId(platform, year, month)]
