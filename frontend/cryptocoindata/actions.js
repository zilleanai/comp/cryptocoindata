import { createRoutine } from 'actions'

export const listPlatforms = createRoutine('cryptocoindata/LIST_PLATFORMS')
export const listYears = createRoutine('cryptocoindata/LIST_YEARS')
export const listMonths = createRoutine('cryptocoindata/LIST_MONTHS')
export const listRecords = createRoutine('cryptocoindata/LIST_RECORDS')
export const startDownload = createRoutine('cryptocoindata/START_DOWNLOAD')